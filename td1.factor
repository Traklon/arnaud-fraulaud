! Copyright (C) 2014 Your name.
! See http://factorcode.org/license.txt for BSD license.
USING: kernel sequences math math.parser io math.primes math.primes.factors vectors ;
IN: td1

: fact ( n -- x ) dup 2 < [ drop 1 ] [ dup 1 - fact * ] if ;

: mov ( x x -- x ) [ number>string ] bi@ " vers " glue ; 

: other ( x x -- x ) + 6 swap - ;

: partial ( x x -- x x ) swap dup [ other ] dip swap ;

: hanoi ( x x x -- ) dup 1 = [ [ mov print ] dip drop ] [ 1 - 3dup [ partial ] dip hanoi [ 2dup mov print ] dip [ swap partial swap ] dip hanoi ] if ;

: ntgf ( n array -- n array ) dup length 0 = [ drop V{ } ] [ [ next-prime dup ] dip dup first first swap [ = ] dip swap [ dup first second 1vector [ 1 tail ntgf ] dip append ] [ ntgf V{ 0 } append ] if ] if ;

: nttmp ( array -- vector ) [ 1 ] dip ntgf [ drop ] dip reverse ;

: recu ( array -- array ) [ dup [ 0 = ] dip swap [ 0 [ drop ] dip ] [ dup [ 1 = ] dip swap [ 1 [ drop ] dip ] [ group-factors nttmp recu ] if ] if ] map ;

: paren ( vector -- string ) [ dup vector? [ paren ] [ number>string ] if ] [ append ] map-reduce "(" ")" surround ;

: >engramme ( n -- string ) dup 2 < [ number>string ] [ group-factors nttmp recu paren ] if ;
