module TD2 where

import Data.Ratio
import Data.Function (on)
import Data.Ord (comparing)
import Data.List (sortBy, groupBy)
import Data.Monoid

data Prob a = Prob [(a,Rational)] deriving (Show)

sameProbability :: [a] -> Prob a
sameProbability l = Prob [(x,1% (toInteger (length l))) | x <- l]

instance Functor Prob where
  fmap f (Prob l) = Prob [(f x, p) | (x,p) <- l]

instance Monad Prob where
  return a = Prob [(a,1%1)]
  (>>=) (Prob l) f = Prob [(v,p1*p2) | (x,p1) <- l, (v,p2) <- fun x]
    where fun x  = let Prob tmp = f x in tmp
  fail a = Prob []

canonize :: (Eq a , Ord a) => Prob a -> Prob a
canonize (Prob l) = Prob ([(fst (e!!0),sum (map snd e)) | e <- tmp])
  where tmp = (groupBy ((==) `on` fst)) ((sortBy (comparing fst)) l)

probability :: Eq a => a -> Prob a -> Rational
probability x (Prob l) = maybe 0 id (lookup x l)

dice :: Prob Int
dice = sameProbability [1..6]

double :: Prob Bool
double = do
  x <- dice
  y <- dice
  return $ x == y

pair :: Prob Int
pair = do
  x <- dice
  y <- dice
  return $ x+y

sick :: Prob Bool
sick = Prob [(True, 1%100000), (False, 99999%100000)]

positive :: Bool -> Prob Bool
positive b = fmap (if b then id else not) (Prob [(True, 999%1000), (False, 1%1000)])

renormalize :: Prob a -> Prob a
renormalize x = Prob [(e,p/s) | (e,p) <- l]
  where Prob l = x
        s = sum $ map snd l

results :: Prob Bool
results = renormalize tmp
  where tmp = do
          s <- sick
          p <- positive True
          if s == p
            then return s
            else fail "foobar"

-- La probabilité d'être malade est 111/11222, on a donc moins d'une
-- chance sur 100 d'être malade après avoir été testé positif !

cleave :: a -> [a -> b] -> [b]
cleave e l = [f e | f <- l]

spread :: [a -> b] -> [a] -> [b]
spread = zipWith ($)

data Writer w a = Writer {runWriter :: (w,a)} deriving (Show)

data Reader w a = Reader {runReader :: w -> a}

data State w a = State {runState :: w -> (a, w)}

instance Functor (Writer w) where
  fmap f (Writer (w,a)) = Writer (w, f a)

instance (Monoid w) => Monad (Writer w) where
  return e = Writer (mempty,e)
  (>>=) (Writer (w,a)) f = let Writer (w1, a1) = f a in Writer (mappend w w1, a1)

instance Functor (Reader w) where
  fmap f (Reader w) = Reader (\x ->  (f (w x)))

instance Monad (Reader w) where
  return = Reader . const 
  Reader r >>= f = Reader (\x -> runReader (f (r x)) x)

instance Functor (State w) where
  fmap f (State x) = State (\w -> let (a, w1) = x w in (f a, w1))

instance Monad (State w) where
  return x = State (\w -> (x, w))
  (State x) >>= f = State $ \w ->
    let (a, w1) = x w in let (State x1) = f a
      in x1 w1

tell :: w -> Writer w ()
tell e = Writer (e, ())

compute :: Writer w x -> x
compute = snd.runWriter

describe :: Writer w x -> w
describe = fst.runWriter

display :: Writer [String] x -> IO ()
display = mapM_ putStrLn . describe

asks :: Eq x => x -> Reader [(x, a)] (Maybe a)
asks k = do
  env <- ask
  return (lookup k env)

ask :: Reader e e
ask = Reader id

put :: w -> State w ()
put x = State (\w -> ((), x))

get :: State w w
get = State (\w -> (w, w))

modify :: (w -> w) -> State w ()
modify f = do
  curr <- get
  put $ f curr
