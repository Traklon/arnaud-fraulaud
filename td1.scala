package fr.enst.plnc2014.td1

import scala.math.sqrt
import ImpComplex._
import ExtSeq._

object TD1 {

  // Placer ici le code de isOdd & friends
  def isOdd(x: Int): Boolean = ((x%2)+2)%2 == 1 
  def isEven(x: Int): Boolean = ((x%2)+2)%2 == 0

  //def myWhile (b: => Boolean, f: => Int) = 

  def possible (q:(Int,Int), qs:List[(Int, Int)]) = { 
    def fq (q1:(Int, Int)): Boolean = if (verif(q,q1)) false else true
    qs.all(fq)
  }

  def verif (q1:(Int, Int), q2:(Int, Int)) = {
    (q1._1 == q2._1) || (q1._2 == q2._2) ||
    (q1._1 - q2._1) == (q1._2 - q2._2) || (q1._1 - q2._1) == -(q1._2 - q2._2)
  }
  
  def f (l:List[(Int, Int)]):Unit = print ("Sol ")

  def solveQueens(numberOfQueens: Int, f: List[(Int, Int)] => Unit): List[List[(Int, Int)]] = {
    def sQ (n: Int):List[List[(Int,Int)]] = {
      if (n == 0)
        return List(Nil)
      else
        for {
          qPrec <- sQ(n-1)
          col <- 1 to numberOfQueens
          q = (n,col)
          if (possible (q,qPrec)) 
        }
        yield (q::qPrec)
    }

    for {i <- sQ(numberOfQueens)}
      f(i)
    sQ(numberOfQueens)
  }
}

class ExtSeq [T] (val l:Seq[T]) {
  def any (f: T => Boolean): Boolean = { 
    for (i <- 0 until l.length) {
        if (f(l(i))) return (true)
    }
    return (false)
  }

  def all (f: T => Boolean): Boolean = {
    for (i <- 0 until l.length) {
      if (!(f(l(i)))) return (false)
    }
    return (true)
  }
}

object ExtSeq {
  implicit def toExtSeq[T] (l: Seq[T]) : ExtSeq[T] = new ExtSeq[T](l)
}


case class Complex (val real:Double, val imag:Double) {
  override def toString ():String = {
    if (imag == 0) return real.toString()
    else {
      if (real == 0) return ((imag.toString())+'i')
      else {
        if (imag < 0) return ((real.toString())+(imag.toString())+'i')
        return ((real.toString())+'+'+(imag.toString())+'i')
      }
    }
  }

  def reciprocal ():Complex = Complex(real,(-1)*imag)
  
  def + (c:Complex):Complex = Complex(real+c.real, imag+c.imag)
  def - (c:Complex):Complex = Complex(real-c.real, imag-c.imag)
  def * (c:Complex):Complex = Complex(real*c.real-imag*c.imag, imag*c.real+real*c.imag)
  def / (c:Complex):Complex = {
    val den:Double = c.real*c.real+c.imag*c.imag
    return Complex((real*c.real+imag*c.imag)/den, (imag*c.real-real*c.imag)/den)
  }
  def abs ():Double = sqrt(real*real+imag*imag)

  def + (c:Double):Complex = Complex(real+c, imag)
}

object ImpComplex {
  implicit def doubleToComplex (real:Double) = Complex(real,0)
}


object Main extends App {
  
  import TD1._

  // Placer ici le code à exécuter
  val c = new Complex (1.0,6.0)
  val c1 = new Complex (2.0,-3.0)
  val c2 = new Complex (1.0,0.0)
  val c3 = new Complex (0.0,6.0)
  val c4 = new Complex (-1.0,6.0)
  println(5+c)

  solveQueens(8,f)

}
