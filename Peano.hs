module Peano where

data Peano = Zero | Succ Peano
  deriving (Show, Read)

instance Num Peano where
  Zero + a = a
  Succ a + b = a+Succ(b)
  a - Zero = a
  Zero - _ = error "Pas de nombres négatifs."
  Succ a - Succ b = a-b
  Zero * a = Zero
  Succ a * b = a*b+b
  signum Zero = Zero
  signum _ = Succ Zero
  abs a = a
  fromInteger 0 = Zero
  fromInteger a = Succ (fromInteger(a-1))

instance Enum Peano where
  toEnum 0 = Zero
  toEnum a = Succ (toEnum(a-1))
instance Real Peano

instance Integral Peano where
  toInteger Zero = 0
  toInteger (Succ a) = 1 + (toInteger a)
  div a b = fromInteger $ div (toInteger a) (toInteger b)

instance Eq Peano where
  (==) Zero Zero = True
  (==) (Succ a) (Succ b) = (==) (Succ a - Succ b) Zero
  (==) _ _ = False
  (/=) a b = not $ (==) a b

instance Ord Peano where
  compare Zero Zero = EQ
  compare (Succ a) Zero = GT
  compare Zero (Succ a) = LT
  compare (Succ a) (Succ b) = compare a b
  (<=) a b = compare a b /= GT
      
















