module RPN2 where

import Prelude hiding (String)
import Data.List.Split
import System.IO

type String = [Char]

type Operator a = [a] -> [a]

func :: (a -> a -> a) -> [a] -> [a]
func f (x:y:s) = (f y x):s

pick :: [a] -> Int -> a 
pick l 0 = head l
pick l n = pick (tail l) (n-1)

parseOp :: (Num a, Eq a, Ord a, Integral a, Read a, Show a) => String -> (Operator a)
parseOp "+" = func (+)
parseOp "-" = func (-)
parseOp "*" = func (*)
parseOp "/" = func div
parseOp "dup" = (\(x:s) -> x:x:s)
parseOp "drop" = tail 
parseOp "swap" = (\(x:y:s) -> y:x:s)
parseOp "pick" = (\(x:s) -> (s!!(fromIntegral(toInteger x))):s)
parseOp "depth" = (\l -> (fromInteger(toInteger(length l))):l)
parseOp x = (read x:)

eval :: (Num a, Eq a, Ord a, Integral a, Read a, Show a) => [a] -> [String] -> [a]
eval l [] = l
eval l (x:s) = eval (parseOp x l) s 

parse :: String -> [String]
parse = splitOn " "

repl :: (Num a, Eq a, Ord a, Integral a, Read a, Show a) => [a] -> IO ()
repl stack = do
  putStr "> "
  hFlush stdout
  line <- getLine
  newstack <- return $ eval stack (parse line)
  putStrLn $ show $ reverse newstack
  repl newstack

main = repl []
