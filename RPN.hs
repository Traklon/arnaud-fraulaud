module RPN where

import Prelude hiding (String)
import Data.List.Split
import System.IO

type String = [Char]

type Stack = [Int]

type Operator = Stack -> Stack

func :: (Int -> Int -> Int) -> Stack -> Stack
func f (x:y:s) = (f y x):s

pick :: Stack -> Int -> Int
pick l 0 = head l
pick l n = pick (tail l) (n-1)

parseOp :: String -> Operator
parseOp "+" = func (+)
parseOp "-" = func (-)
parseOp "*" = func (*)
parseOp "/" = func div
parseOp "dup" = (\(x:s) -> x:x:s)
parseOp "drop" = tail 
parseOp "swap" = (\(x:y:s) -> y:x:s)
parseOp "pick" = (\(x:s) -> (s!!x):s)
parseOp "depth" = (\l -> (length l):l)
parseOp x = (read x:)

eval :: Stack -> [String] -> Stack
eval l [] = l
eval l (x:s) = eval (parseOp x l) s 

parse :: String -> [String]
parse = splitOn " "


repl :: Stack -> IO ()
repl stack = do
  putStr "> "
  hFlush stdout
  line <- getLine
  newstack <- return $ eval stack (parse line)
  putStrLn $ show $ reverse newstack
  repl newstack

main = repl []
