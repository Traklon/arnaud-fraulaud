! Copyright (C) 2014 Your name.
! See http://factorcode.org/license.txt for BSD license.

USING: locals math math.parser arrays io kernel sequences math.combinatorics formatting ;
IN: td2

: ens ( n -- array ) dup 0 = [ drop { } ] [ 1 - dup [ ens ] dip 1array append ] if ;

:: isSafe ( e q -- b ) q e nth :> x q ens [ x swap dup [ e nth ] dip q swap - [ + = not ] [ - = not ] 3bi and ] all? ;

: queens ( i -- l ) ens all-permutations [ dup length ens [ [ dup ] dip isSafe ] all? [ drop ] dip ] filter ;

: draw ( n -- ) dup 0 = [ drop "X" print ] [ " " printf 1 - draw ] if ;

: display ( n -- ) queens [ dup [ 1 + number>string print ] each nl [ draw ] each nl nl ] each ;
